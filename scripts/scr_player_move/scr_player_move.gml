///scr_player_move(max_speed, acceleration, friction, W-key, A-key, S-key, D-key);
///@author Freek Jansen

if(!global.oneTimeUse){
	total_vspeed = 0;
	total_hspeed = 0;
	global.oneTimeUse = true;
} else {
	//VERTICAL MOVEMENT
	if(keyboard_check(argument3) && !keyboard_check(argument5)){
		total_vspeed -= argument1;
	} else if(keyboard_check(argument5) && !keyboard_check(argument3)){
		total_vspeed += argument1;	
	} else {	
		//APPLYING FRICTION
		if(total_vspeed > 0){
			total_vspeed -= argument2;	
		} else {
			total_vspeed = 0;
		}
	
		if(total_vspeed < 0){
			total_vspeed += argument2;	
		} else {
			total_vspeed = 0;
		}
	}

	//HORIZONTAL MOVEMENT
	if(keyboard_check(argument4) && !keyboard_check(argument6)){
		total_hspeed -= argument1;
	} else if(keyboard_check(argument6) && !keyboard_check(argument4)){
		total_hspeed += argument1;	
	} else {	
		//APPLYING FRICTION
		if(total_hspeed > 0){
			total_hspeed -= argument2;	
		} else {
			total_hspeed = 0;
		}
	
		if(total_hspeed < 0){
			total_hspeed += argument2;	
		} else {
			total_hspeed = 0;
		}
	}
	
	//COLLISIONS
	if(place_meeting(x+total_hspeed, y, par_solid)){
		while(!place_meeting(x+sign(total_hspeed), y, par_solid)){
			x += sign(total_hspeed)	
		}
		total_hspeed = 0;
	}
	
	if(place_meeting(x, y+total_vspeed, par_solid)){
		while(!place_meeting(x, y+sign(total_vspeed), par_solid)){
			y += sign(total_vspeed)
		}
		total_vspeed = 0;
	}

	//CLAMPING THE SPEEDS
	total_hspeed = clamp(total_hspeed, -argument0, argument0);
	total_vspeed = clamp(total_vspeed, -argument0, argument0);

	//APPLYING COORDINATE CHANGES
	x += total_hspeed;
	y += total_vspeed;
}


