///scr_player_animate(spr_idle, spr_running, hSpeed, vSpeed, run_speed, idle_speed);
///@author Freek Jansen

if(argument2 > 0){
	sprite_index = argument1;	
	image_xscale = -1;
	image_speed = argument4;
} else if(argument2 < 0){
	sprite_index = argument1;	
	image_xscale = 1;
	image_speed = argument4;
} else if(argument3 != 0){
	sprite_index = argument1;
	image_speed = argument4;
} else {
	sprite_index = argument0;
	image_speed = argument5;	
}

depth = -y;