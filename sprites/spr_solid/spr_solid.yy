{
    "id": "320a8a16-8167-4c94-9051-9be442c9fb4c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_solid",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 32,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "47a37cba-4896-4a37-bdfe-f665a6bdab57",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320a8a16-8167-4c94-9051-9be442c9fb4c",
            "compositeImage": {
                "id": "3fc82dce-82dc-4f7d-bed2-d75ddbebab8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47a37cba-4896-4a37-bdfe-f665a6bdab57",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba85dfc1-b2b8-404e-852a-c2f379d8a683",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47a37cba-4896-4a37-bdfe-f665a6bdab57",
                    "LayerId": "c4e7cd1f-7074-44ad-a412-261946730929"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "c4e7cd1f-7074-44ad-a412-261946730929",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "320a8a16-8167-4c94-9051-9be442c9fb4c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 32
}