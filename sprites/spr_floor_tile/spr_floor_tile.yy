{
    "id": "8aee8364-3e40-4c03-b320-6bdfb7477211",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_floor_tile",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "193fabcc-8e0b-4474-a029-8cd8152185fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8aee8364-3e40-4c03-b320-6bdfb7477211",
            "compositeImage": {
                "id": "9df6ccaa-42d2-425e-a2b6-02d747e589f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "193fabcc-8e0b-4474-a029-8cd8152185fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c08f0191-994f-4e1d-8d20-139a35932a51",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "193fabcc-8e0b-4474-a029-8cd8152185fc",
                    "LayerId": "09794550-fb1b-4bb9-ae27-4d924c59cc0d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "09794550-fb1b-4bb9-ae27-4d924c59cc0d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8aee8364-3e40-4c03-b320-6bdfb7477211",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}