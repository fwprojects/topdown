{
    "id": "8f99953e-404e-4432-93d8-c9c29721fe89",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_mask",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 11,
    "bbox_right": 19,
    "bbox_top": 20,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "685bfb93-933a-4a9a-9c60-ea4e6f76bfad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8f99953e-404e-4432-93d8-c9c29721fe89",
            "compositeImage": {
                "id": "852156dd-d824-4732-971a-33cf717ceda3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "685bfb93-933a-4a9a-9c60-ea4e6f76bfad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e47c8d2e-7cf9-44e0-9f77-3f32756b4671",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "685bfb93-933a-4a9a-9c60-ea4e6f76bfad",
                    "LayerId": "9c69085b-4b81-41f9-ba9f-b4ce880f1c67"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9c69085b-4b81-41f9-ba9f-b4ce880f1c67",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8f99953e-404e-4432-93d8-c9c29721fe89",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 15,
    "yorig": 17
}