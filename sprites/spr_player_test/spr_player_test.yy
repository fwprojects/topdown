{
    "id": "6cc3c258-a471-4037-bf06-720a31c66518",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_test",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5da73ff1-eca7-43db-ac72-ac51e29d6d2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cc3c258-a471-4037-bf06-720a31c66518",
            "compositeImage": {
                "id": "0609f30a-6180-4fe3-b99f-ee78e735366f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5da73ff1-eca7-43db-ac72-ac51e29d6d2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5b8d5ff-3a1e-4187-b110-0104f5ea99a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5da73ff1-eca7-43db-ac72-ac51e29d6d2c",
                    "LayerId": "a5ce5c71-7a35-4f99-a008-4acf1353b9c1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a5ce5c71-7a35-4f99-a008-4acf1353b9c1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6cc3c258-a471-4037-bf06-720a31c66518",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}