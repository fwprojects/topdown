{
    "id": "50554a92-8c39-403b-97c3-4431a2b77e91",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wall_tile",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 159,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "412ababf-5900-450d-981f-48fea2d7b446",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50554a92-8c39-403b-97c3-4431a2b77e91",
            "compositeImage": {
                "id": "12355cda-fcfe-4273-b4d1-ad3217e030e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "412ababf-5900-450d-981f-48fea2d7b446",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7ab0af6-f598-4601-8c1c-2d63ccb57117",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "412ababf-5900-450d-981f-48fea2d7b446",
                    "LayerId": "9f80cc94-c4b0-4aa0-8a0d-dcbf61403662"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "9f80cc94-c4b0-4aa0-8a0d-dcbf61403662",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "50554a92-8c39-403b-97c3-4431a2b77e91",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 160,
    "xorig": 0,
    "yorig": 0
}