{
    "id": "dbc1ccf8-e798-4b00-ac04-e610e60b0543",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_move",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 10,
    "bbox_right": 20,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a1b56a76-3417-4cd1-ad34-dceca0784d46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dbc1ccf8-e798-4b00-ac04-e610e60b0543",
            "compositeImage": {
                "id": "ed2e5e9f-57bd-49de-bef6-fae791babe23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1b56a76-3417-4cd1-ad34-dceca0784d46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "981ffca4-b901-432f-9740-ffaed2cf4932",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1b56a76-3417-4cd1-ad34-dceca0784d46",
                    "LayerId": "b24fbad4-5c84-4570-8c01-1440877777dc"
                }
            ]
        },
        {
            "id": "5693d132-39d1-4fd2-aecf-b5d1c9cfef4d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dbc1ccf8-e798-4b00-ac04-e610e60b0543",
            "compositeImage": {
                "id": "e26de292-0f35-45e5-ad4b-f07f3c2cbb2d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5693d132-39d1-4fd2-aecf-b5d1c9cfef4d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d06c6d0-6086-448a-bb9a-7aae51516106",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5693d132-39d1-4fd2-aecf-b5d1c9cfef4d",
                    "LayerId": "b24fbad4-5c84-4570-8c01-1440877777dc"
                }
            ]
        },
        {
            "id": "f4bf1888-d3e9-4bad-9934-1802867a4cda",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dbc1ccf8-e798-4b00-ac04-e610e60b0543",
            "compositeImage": {
                "id": "b8089ee0-d7ab-4d16-919e-74d4a8cabcdc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4bf1888-d3e9-4bad-9934-1802867a4cda",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9898944c-933c-4ea5-ae06-09a378e9093c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4bf1888-d3e9-4bad-9934-1802867a4cda",
                    "LayerId": "b24fbad4-5c84-4570-8c01-1440877777dc"
                }
            ]
        },
        {
            "id": "dd080319-4b6c-45c6-a2a5-ec74f2928ef6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dbc1ccf8-e798-4b00-ac04-e610e60b0543",
            "compositeImage": {
                "id": "2677b4e7-debf-4e35-a35f-0072bfa3a56e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd080319-4b6c-45c6-a2a5-ec74f2928ef6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32cf3db2-6f7d-483a-a6a8-56a3748dddb5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd080319-4b6c-45c6-a2a5-ec74f2928ef6",
                    "LayerId": "b24fbad4-5c84-4570-8c01-1440877777dc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b24fbad4-5c84-4570-8c01-1440877777dc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dbc1ccf8-e798-4b00-ac04-e610e60b0543",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 15,
    "yorig": 17
}