{
    "id": "373560b4-4575-4f4c-a75c-85afc3780534",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 10,
    "bbox_right": 20,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d93688f4-fe82-4dff-b40c-f3308dbb16b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "373560b4-4575-4f4c-a75c-85afc3780534",
            "compositeImage": {
                "id": "9673b7a4-9cbd-44de-bde7-3d911e6f6cc7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d93688f4-fe82-4dff-b40c-f3308dbb16b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b5a2c39-0ee1-4de1-8b86-dc95281cfaff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d93688f4-fe82-4dff-b40c-f3308dbb16b2",
                    "LayerId": "459a3e17-2216-4a20-bd82-eaad8d4be046"
                }
            ]
        },
        {
            "id": "4d24cedf-504b-4d97-a947-4e84b74680fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "373560b4-4575-4f4c-a75c-85afc3780534",
            "compositeImage": {
                "id": "a6d65c5c-b10c-4712-aaf2-1ca369f23892",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d24cedf-504b-4d97-a947-4e84b74680fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0036fc20-c6f1-40c1-8657-3cd9c5c2c266",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d24cedf-504b-4d97-a947-4e84b74680fe",
                    "LayerId": "459a3e17-2216-4a20-bd82-eaad8d4be046"
                }
            ]
        },
        {
            "id": "e24e7425-c5f6-46b3-b37d-9dc0048d0760",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "373560b4-4575-4f4c-a75c-85afc3780534",
            "compositeImage": {
                "id": "8c6aace5-5341-4c4d-91cb-7ed7e9a8b443",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e24e7425-c5f6-46b3-b37d-9dc0048d0760",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22dc152a-911a-4eb7-86c6-cf308c1041ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e24e7425-c5f6-46b3-b37d-9dc0048d0760",
                    "LayerId": "459a3e17-2216-4a20-bd82-eaad8d4be046"
                }
            ]
        },
        {
            "id": "412237fc-bd63-4f2e-8e11-c1831012937c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "373560b4-4575-4f4c-a75c-85afc3780534",
            "compositeImage": {
                "id": "110f72ab-5a05-400a-b876-76383fd5779b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "412237fc-bd63-4f2e-8e11-c1831012937c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "540a5531-8d18-459e-8d13-f2e4cc4d7991",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "412237fc-bd63-4f2e-8e11-c1831012937c",
                    "LayerId": "459a3e17-2216-4a20-bd82-eaad8d4be046"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "459a3e17-2216-4a20-bd82-eaad8d4be046",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "373560b4-4575-4f4c-a75c-85afc3780534",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 15,
    "yorig": 17
}