{
    "id": "9508484a-b632-4ab6-a1a7-22d99bb186ec",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_level",
    "eventList": [
        {
            "id": "3012b11e-be41-4450-8c94-3d3d771891cc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9508484a-b632-4ab6-a1a7-22d99bb186ec"
        },
        {
            "id": "3ba2de23-9149-44de-b7e7-f48f66cd528b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 82,
            "eventtype": 5,
            "m_owner": "9508484a-b632-4ab6-a1a7-22d99bb186ec"
        },
        {
            "id": "89951070-97ff-4d86-b702-f0c58767e5cf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "9508484a-b632-4ab6-a1a7-22d99bb186ec"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}