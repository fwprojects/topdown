//Step event Player
//@author Wouter van der Heijden

scr_player_move(max_speed, accel, fric, keyUp, keyLeft, keyDown, keyRight);
scr_player_animate(spr_player_idle, spr_player_move, total_hspeed, total_vspeed, run_speed, idle_speed);

if(keyboard_check_pressed(ord("R"))){
	game_restart();	
}