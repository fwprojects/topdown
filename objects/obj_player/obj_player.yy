{
    "id": "22b38d9d-85c3-43a0-b9ca-b62fac29dfd6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player",
    "eventList": [
        {
            "id": "5955a3aa-fca7-4d3c-a7ce-230d5f6b7416",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "22b38d9d-85c3-43a0-b9ca-b62fac29dfd6"
        },
        {
            "id": "6e4fd54a-cb3c-4f80-9a5d-ba65d7ffd4cb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "22b38d9d-85c3-43a0-b9ca-b62fac29dfd6"
        }
    ],
    "maskSpriteId": "8f99953e-404e-4432-93d8-c9c29721fe89",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "373560b4-4575-4f4c-a75c-85afc3780534",
    "visible": true
}