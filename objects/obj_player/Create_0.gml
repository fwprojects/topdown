//Create event Player
//@author Wouter van der Heijden

max_speed = 2;
fric = 0.6;
accel = 0.3;

//ANIMATION SPEEDS
run_speed = 1.0;
idle_speed = 0.7;

global.oneTimeUse = false;

keyUp = ord("W");
keyLeft = ord("A");
keyDown = ord("S");
keyRight = ord("D");
