{
    "id": "1c10813f-3583-4fcf-afc7-7c682804f485",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "par_solid",
    "eventList": [
        {
            "id": "f6c50dd9-b8e1-4402-949b-1e8e460c5bf4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1c10813f-3583-4fcf-afc7-7c682804f485"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "320a8a16-8167-4c94-9051-9be442c9fb4c",
    "visible": true
}