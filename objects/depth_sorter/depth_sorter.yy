{
    "id": "9b99ea11-fe2e-4721-80c6-a83f64f6006c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "depth_sorter",
    "eventList": [
        {
            "id": "78cd364d-fe78-41c1-8dee-4eff2e7063aa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9b99ea11-fe2e-4721-80c6-a83f64f6006c"
        },
        {
            "id": "1040e0d0-2128-45b4-97fd-a17e6ce63ede",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "9b99ea11-fe2e-4721-80c6-a83f64f6006c"
        },
        {
            "id": "1bd86163-ec90-4690-a1e1-b0e839bd96cc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "9b99ea11-fe2e-4721-80c6-a83f64f6006c"
        },
        {
            "id": "a3af1250-cd4f-477d-837e-d238516a49c7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "9b99ea11-fe2e-4721-80c6-a83f64f6006c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}